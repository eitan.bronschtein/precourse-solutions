//Your task is to write function cipher that converts a regular English sentence to ciphered text.

    // Consider only uppercase letters (no lowercase letters, no numbers) and spaces.

    // For example:

    // cipher("I HAVE A SECRET MESSAGE FOR YOU")  returns "O IQCT Q LTEKTZ DTLLQUT YGK NGX"

// After you've finished the cipher part add a new function to decipher back to English

// In this exercise we use a simple encoding mapping with English alphabet in the left column
// and ciphered matches in the left column:

encrypt = {

    A: "Q",
    B: "W",
    C: "E",
    D: "R",
    E: "T",
    F: "Y",
    G: "U",
    H: "I",
    I: "O",
    J: "P",
    K: "A",
    L: "S",
    M: "D",
    N: "F",
    O: "G",
    P: "H",
    Q: "J",
    R: "K",
    S: "L",
    T: "Z",
    U: "X",
    V: "C",
    W: "V",
    X: "B",
    Y: "N",
    Z: "M"
};

decrypt = {
    Q: 'A',
    W: 'B',
    E: 'C',
    R: 'D',
    T: 'E',
    Y: 'F',
    U: 'G',
    I: 'H',
    O: 'I',
    P: 'J',
    A: 'K',
    S: 'L',
    D: 'M',
    F: 'N',
    G: 'O',
    H: 'P',
    J: 'Q',
    K: 'R',
    L: 'S',
    Z: 'T',
    X: 'U',
    C: 'V',
    V: 'W',
    B: 'X',
    N: 'Y',
    M: 'Z'
};

function cipher(message) {

    let encrypted_message = "";

    for(let i = 0; i < message.length; i++) {
        if (encrypt[message[i]] === undefined) {
            encrypted_message += message[i];
        } else {
            encrypted_message += encrypt[message[i]];
        }
    }

    return encrypted_message;
}

function decipher(message) {
    let decrypted_message = "";

    for(let i = 0; i < message.length; i++) {
        if (decrypt[message[i]] === undefined) {
            decrypted_message += message[i];
        } else {
            decrypted_message += decrypt[message[i]];
        }
    }

    return decrypted_message;
}

console.log(cipher("I HAVE A SECRET MESSAGE FOR YOU"));
console.log(decipher(cipher("I HAVE A SECRET MESSAGE FOR YOU")));