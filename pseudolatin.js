// Pseudo Latin

// Create a function which takes a string as an argument and
// moves the first letter of each word to the end of it, then adds "ay"
// to the end of the word. Leave punctuation marks untouched.

//makeLatin("I speak latin")
// should return:
//"Iay peaksay atinlay"

let latinzie_word = word => {
    return word.slice(1) + word[0] + "ay";
}

let pseudo_latin = sentence => {
    let words = sentence.split(' ');
    return words.map(latinzie_word).join(" ");
}
console.log(pseudo_latin("I speak latin"));
