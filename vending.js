/*
write a function called vendingMachine
it should take 2 arguments:
– number for the amount of money entered
– number which refers to a snack you want to buy

It should return
"You -snack- has been served", instead of a -snack- it should output
a name of selected snack

If selected snack is not available in the vending machine it should return
"Sorry, selected snack is not available"

If selected snack is more expensive than the money amount given it should return
"Sorry, you have to insert more coins"

This is the content of our vending machine:
1. Espresso, cost: 1€
2. Cappuccino, cost: 2,50€
3. Chocolate bar, cost 2€
4. Potato chips, cost 3,50€
*/

let machine_content = {
    1: {name: "espresso", price: 1},
    2: {name: "cappuccino", price: 2.5},
    3: {name: "chocolate_bar", price: 2},
    4: {name: "potate_chips", cost: 3.5}

};

function vending_machine(choice, money) {

    let more_money = "Sorry, you have to insert more coins";

    switch(choice) {
        case 1:
            if (money >= machine_content[1].price) {
                return "Your " + machine_content[choice].name + " is being served.";
            } else {
                return more_money;
            }
        case 2:
            if (money >= machine_content[2].price) {
                return "Your " + machine_content[choice].name + " is being served.";
            } else {
                return more_money;
            }
        case 3:
            if (money >= machine_content[3].price) {
                return "Your " + machine_content[choice].name + " is being served.";
            } else {
                return more_money;
            }
        case 4:
            if (money >= machine_content[4].price) {
                return "Your " + machine_content[choice].name + " is being served.";
            } else {
                return more_money;
            }
        default:
            return "sorry snack not available";
    }
}




console.log(vending_machine(1,5));
console.log(vending_machine(7,5));
console.log(vending_machine(2,0.3));