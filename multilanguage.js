// write a function which takes two argument: name and the language and it
// returns a personalized greeting in the selected language
// If no language is provided it defaults to English.

dictionary = {
    english: 'Welcome',
    czech: 'Vitejte',
    danish: 'Velkomst',
    dutch: 'Welkom',
    estonian: 'Tere tulemast',
    finnish: 'Tervetuloa',
    flemish: 'Welgekomen',
    french: 'Bienvenue',
    german: 'Willkommen',
    irish: 'Failte',
    italian: 'Benvenuto',
    latvian: 'Gaidits',
    lithuanian: 'Laukiamas',
    polish: 'Witamy',
    spanish: 'Bienvenido',
    swedish: 'Valkommen',
    welsh: 'Croeso'
}

function greeting(name, language="english") {
    return dictionary[language] + ", " + name;
}
// example:
console.log(greeting("bob", "spanish"))
console.log(greeting( "you"));
