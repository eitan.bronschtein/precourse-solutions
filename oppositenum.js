// write a function which returns the opposite for a provided number:

function oppositeNum(num) {
    return (-1 * num);
}

console.log(oppositeNum(10));
console.log(oppositeNum(-5));
