
function swap(a) {
    var keys = Object.keys(a);
    var swapped = {};

    keys.forEach( key => (
        swapped[a[key]] = key
    ));
    return swapped;
}

console.log(swap({a: 1, b: 2, c: 3}))
console.log(swap({1: 2, 2: 3, 3: 4}))
console.log(swap({

    A: "Q",
    B: "W",
    C: "E",
    D: "R",
    E: "T",
    F: "Y",
    G: "U",
    H: "I",
    I: "O",
    J: "P",
    K: "A",
    L: "S",
    M: "D",
    N: "F",
    O: "G",
    P: "H",
    Q: "J",
    R: "K",
    S: "L",
    T: "Z",
    U: "X",
    V: "C",
    W: "V",
    X: "B",
    Y: "N",
    Z: "M"
}
));